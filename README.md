This project focused on reading in 15 minute cumulative rainfall (inches) data from a site in Hyde County, NC. The goal of this project was to break the massive 
36000 + row file into events where rainfall was occurring, and further distill them down to storm events (>= .5 in rain in 24 hr period). This data was manipulated in R to identify rainfall events, and put them in an ordered sequence. 
The resulting file went from 36000 rows to less than 900. This file was exported into an excel spreadsheet where the greatly reduced dataset was able to be 
visually inspected and storm events were determined. The aspects of this project I learned in class include using R studio and loops to manipulate datasets, 
and combining tools (excel) for smooth performance.